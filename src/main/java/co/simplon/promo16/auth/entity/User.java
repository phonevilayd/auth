package co.simplon.promo16.auth.entity;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/**
 * Pour que Spring considère cette classe comme étant un User pour de vrai, il faut
 * que la classe implémente l'interface UserDetails de Spring qui contient un certain
 * nombre de méthodes essentiels pour lui (récupérer les rôles du user, récupérer son username, son mot de passe etc.)
 */
public class User implements UserDetails{
    private Integer id;
    private String email;
    private String password;
    private String role;
    public User(String email, String password, String role) {
        this.email = email;
        this.password = password;
        this.role = role;
    }
    public User(Integer id, String email, String password, String role) {
        this.id = id;
        this.email = email;
        this.password = password;
        this.role = role;
    }
    public User() {
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    /**
     * Ici pour les rôles, on va considérer que chaque user ne peut avoir qu'un
     * seul rôle et on va donc renvoyer le rôle stocké en base de donnée pour ce 
     * User sous un format que Spring souhaite, à savoir un tableau de SimpleGrantedAuthority
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        
        return List.of(new SimpleGrantedAuthority(role));
    }
    /**
     * Ici pour le getUsername, on renvoie l'email car on a considéré que notre email
     * serait notre username
     */
    @Override
    public String getUsername() {
        return email;
    }
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }
    @Override
    public boolean isEnabled() {
        return true;
    }
}
