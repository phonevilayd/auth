package co.simplon.promo16.auth.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.auth.entity.User;

/**
 * Ce repository nous permettra de rajouter un user en base de données, mais aussi
 * de récupérer un User par son email, afin de vérifier si un email est déjà pris à l'inscription
 * ou de récupérer un User dans le cas d'un login
 * On lui fait également implémenter l'interface UserDetailsService qui rajoute une
 * méthode loadUserByUsername dont Spring a besoin pour récupérer un User
 */
@Repository
public class UserRepository implements UserDetailsService {
    @Autowired
    private DataSource dataSource;
    /**
     * Méthode qui ajoute un user en base de données
     * @param user Le user à ajouter en base de données
     * @return true si ça à marcher, false sinon
     */
    public boolean save(User user) {
        try {
            //Ici on utilise le DataSourceUtils pour récupérer une connection existante
            // et donc ne pas avoir besoin de close la connection à chaque requête
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("INSERT INTO user (email,password,role) VALUES (?,?,?)",
                            PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, user.getEmail());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getRole());
            // cette partie sert à récupérer la primary key auto incrémenté et à l'assigner
            // à l'instance de User qu'on vient de faire persister
            if (stmt.executeUpdate() == 1) {
                ResultSet result = stmt.getGeneratedKeys();
                result.next();
                user.setId(result.getInt(1));

                return true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
        }

        return false;
    }
    /**
     * Méthode permettant de récupérer un user en se basant sur l'email
     * @param email l'email de l'user recherché
     * @return Le user correspondant à l'email ou null si pas de user
     */
    public User findByEmail(String email) {
        try {
            PreparedStatement stmt = DataSourceUtils.getConnection(dataSource)
                    .prepareStatement("SELECT * FROM user WHERE email=?");
            stmt.setString(1, email);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return new User(
                        result.getInt("id"),
                        result.getString("email"),
                        result.getString("password"),
                        result.getString("role"));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return null;
    }

    /**
     * La méthode de l'interface pour Spring, au final dedans on ne fait qu'appeler notre
     * méthode findByEmail (vu que dans notre cas notre email est notre username)
     * @param username Le username (ici l'email) du user recherché
     * @return Le User correspondant à l'email
     * @throws UsernameNotFoundException On fait en sorte de déclencher une exception si aucun User ne correspond à l'email donné (c'est spring qui veut ça)
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = findByEmail(username);
        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        return user;
    }

}
