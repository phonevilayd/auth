package co.simplon.promo16.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {

    @Autowired
    private PasswordEncoder encoder;

    @GetMapping("/register")
    public String showRegister() {
        System.out.println(encoder.encode("1234"));

        
        return "register";
    }
    
}
