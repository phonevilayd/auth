DROP TABLE IF EXISTS `user`;

CREATE TABLE `user`(  
    id int PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    email VARCHAR(255) UNIQUE NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
) DEFAULT CHARSET UTF8 COMMENT '';


-- Ajoute un user avec comme username test@test.com et comme password 1234
INSERT INTO user (email,password,role) VALUES ("test@test.com", "$2a$10$ekRvhHNe6oHW2UZHK3qsXuWYlV90jo42Q3iJYydpI.6OgmfMZSTVa", "ROLE_USER");